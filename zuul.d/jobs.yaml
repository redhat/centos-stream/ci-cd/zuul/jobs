- job:
    name: mock-build
    description: |
      Job to build rpm(s) from a distgit repository using mock.

      The job provides a "repo" artifact. The artifact can be used
      by child jobs. The job requires a "repo" artifact meaning that
      this job will include dependent "repo" artifacts in the mock
      build if any. Thus this job supports RPM Build requirements
      dependencies exposed by one or multiple "Depends-on".

      Responds to these variables:

      .. zuul:jobvar:: mock_config
         : default: {{ zuul.branch }}-x86_64

         Mock config from /etc/mock/.

      .. zuul:jobvar:: release
         : default: {{ zuul.branch }}

         Branch for centpkg to setup a git repo for sources.

      .. zuul:jobvar:: repos
         : default: []

         A list of rpm repositories to be exposed on build
         node and mock build root. eg.
         {name: 'nightly', url: 'scheme://...', gpgcheck: 1}

    pre-run:
      - playbooks/pre-mock-build.yaml
      - playbooks/attach-nvme.yaml
      - playbooks/ensure-mock-on-nvme.yaml
      - playbooks/ensure-zuul-output-on-nvme.yaml
    run: playbooks/mock-build.yaml
    post-run: playbooks/repo-fetch.yaml
    branches:
      - c10s
      - c9s
    requires:
      - repo
    provides:
      - repo
    nodeset: fedora-40-vm-aws-ssd-xlarge
    timeout: 7200
    roles:
      - zuul: zuul-distro-jobs
    vars:
      no_fetch_sources: true
      localrepo_dir: /data/chainrepo
      release: "{{ zuul.branch }}"
      mock_config: "{{ zuul.branch }}-x86_64"

- job:
    name: rpm-rpminspect
    description: |
      Job to perform a rpminspect run between last published package
      and built artifacts.

      The job requires a "repo" artifact. It expect a repository built by
      the mock-build job.

      Responds to these variables:

      .. zuul:jobvar:: target

    pre-run: playbooks/attach-nvme.yaml
    run: playbooks/rpminspect.yaml
    post-run: playbooks/rpminspect-report.yaml
    branches:
      - c9s
    requires:
      - repo
    timeout: 3600
    nodeset: fedora-40-vm-aws-ssd
    roles:
      - zuul: zuul-distro-jobs
    vars:
      workdir: "/data/rpminspect"
      target: "{{ zuul.branch }}-candidate"
      rpminspect_opts: "-Dv -k -t VERIFY --profile=centos-stream-{{ zuul.branch[1] }}-devel"
